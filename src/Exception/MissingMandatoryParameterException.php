<?php

namespace Drupal\multistep_form_framework\Exception;

/**
 * Missing mandatory parameter exception.
 */
class MissingMandatoryParameterException extends \InvalidArgumentException {}
