<?php

namespace Drupal\multistep_form_framework_examples\Form\BuyBookStep;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Congratulation book step.
 */
class Congratulation extends BaseStep {

  /**
   * {@inheritDoc}
   */
  protected function getTitle(): string {
    return $this->t('Congratulation!');
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['description'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Go back to our site! Thank you.'),
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function getActions(array $form, FormStateInterface $form_state): array {
    $actions = parent::getActions($form, $form_state);
    // Remove back button.
    unset($actions[self::BACK]);

    $actions[self::SUBMIT]['#value'] = $this->t('Go back to site.');
    $actions[self::SUBMIT]['#ajax']['callback'] = [
      static::class,
      'redirectToFrontPage',
    ];
    return $actions;
  }

  /**
   * Ajax callback that returns redirect command.
   */
  public static function redirectToFrontPage() {
    return (new AjaxResponse())->addCommand(
      new RedirectCommand(Url::fromRoute('<front>')->toString())
    );
  }

}
